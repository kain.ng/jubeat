const fs = require('fs');
var diff = JSON.parse(fs.readFileSync('./EasyStandard.dat', "utf8"));
var info = JSON.parse(fs.readFileSync('./info.dat', "utf8"));

let BPM = info._beatsPerMinute
output_notes = []
for (let i = 0; i < diff._notes.length; i++){
	let note = {
		time: diff._notes[i]._time / BPM * 60000,
		pos: (2-diff._notes[i]._lineLayer)*4 + diff._notes[i]._lineIndex
	}
	output_notes.push(note)
}
let outstring = "";
for (let i = 0; i < output_notes.length; i++){
	outstring += Math.round(output_notes[i].time).toString().padStart(7,'0');
	outstring += Math.round(output_notes[i].pos).toString().padStart(2,'0');
	outstring += "\n";
}
outstring = outstring.slice(0,-1)
console.log(outstring)
fs.writeFileSync('convert.song', outstring)