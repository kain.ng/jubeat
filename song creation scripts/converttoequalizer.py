import numpy as np
from scipy import signal
from scipy.io import wavfile
sample_rate, samples = wavfile.read(sys.argv[1])
frequencies, times, spectrogram = signal.spectrogram(samples, sample_rate)
spectrogram = spectrogram - np.expand_dims(spectrogram.min(axis=1).transpose(), axis=1)

data = spectrogram[0:2,:].sum(axis=0)
new_times = []
step = 0.03
time_step = 0
new_data = []
count = 0
total = 0
for i in range(0,len(data)):
    if (times[i]>step*time_step):
        if count:
            new_data.append(total/count)
        new_times.append(step*time_step)
        time_step += 1
        total = data[i]
        count = 1
    else:
        total += data[i]
        count += 1
new_data = (new_data - min(new_data))/(max(new_data) - min(new_data))
new_data *= 63
new_data = np.array(new_data).round().astype(int)

f = open("output.freq", "w")
for nd in new_data:
    f.write(str(nd)+"\n")
f.close()