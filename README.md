# Jubeat

This is a music game we developed for our CMPE244 class on Fall 2020.

# Intro
Jubeat is a music game uses an arrangement of a 3x4 grid for the game play. An animation pops up on each grid according to the beat of the song. The player has to use his fingers to tap on the screen and catch the animation in time. Different scores will be given depending on when the player catch the animation. Tapes can be judged as PERFECT, OK, and MISS.


[Project Demo Youtube Video](https://www.youtube.com/watch?v=nj6_2ywDC9Y&feature=youtu.be&ab_channel=TylerTran)


To run the software, read [README-SCons.md](https://gitlab.com/kain.ng/jubeat/-/blob/master/sjtwo-c-master/SConstruct) for instructions.

For more detail on how to build this product, please visit our wiki page http://socialledge.com/sjsu/index.php/F20:_Jubeat

