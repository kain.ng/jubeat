/* Note
set the audio volume back to 30 from audio__init()

*/
#include "FreeRTOS.h"

#include "audio.h"
#include "board_io.h"
#include "common_macros.h"
#include "event_groups.h"
#include "ff.h"
#include "gpio.h"
#include "gpio_int.h"
#include "i2c.h"
#include "led_display.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"
#include "periodic_scheduler.h"
#include "pwm1.h"
#include "queue.h"
#include "semphr.h"
#include "sj2_cli.h"
#include "task.h"
#include "uart.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
/* Constant Variables for capsense */
const uint8_t MPR121_TOUCHTH_0reg = 0x41;
const uint8_t MPR121_RELEASETH_0reg = 0x42;
const uint8_t touch_TH_val = 6;
const uint8_t release_TH_val = 3;
QueueHandle_t cap_touch_queue;
SemaphoreHandle_t update_score;
SemaphoreHandle_t cap_touch_detected;
SemaphoreHandle_t start_beat;
SemaphoreHandle_t square_anim_start[12];
SemaphoreHandle_t square_anim_touch[12];
SemaphoreHandle_t active_event_mutex;
SemaphoreHandle_t game_start;
EventGroupHandle_t button_events;
static uint32_t active_events[12];

static uint32_t song_time;
uint32_t score;
static uint8_t game_state;
static int timing;
/* GAME STATES
0 - menu
1 - in game
2 - end
3 - start animation
4 - end animation*/
static TCHAR song_name[20];
static TCHAR song_file[20];
static TCHAR freq_file[20];
static uint8_t song_number = 1;
static uint8_t volume = 30;

/* TASK PROTOTYPES */

static void game_loop_task(void *param);
static void square_anim_task(void *param);
static void display_score_task(void *param);
static void freq_task(void *param);

/* FUNCTION PROTOTYPES*/
void setup_audio();
FIL open_song_data(char *filename);
bool get_next_song_line(uint32_t *time, uint8_t *button, FIL *file);
bool get_freq_next_line(uint8_t *value, FIL *file);
void load_event(uint32_t time, uint8_t button);
void compare_touch_to_event(uint8_t button);
void touched(uint8_t button);
void gameplay_countdown(void);
void start_menu();
void end_screen(uint16_t event_count);
bool isPerfect(uint8_t button);
void getSongInfo(uint8_t selection);
void get_score(int *score_h);
void save_score(int score_h);
void transition(uint8_t color);
void draw_go_button(position p, uint8_t frame_color, uint8_t go_color);
void capsense_reset(void);
void volume_fade_out(void);
/*capsense logic*/
static void i2c2_pin_config(void); // for capsense @P4.28 & P4.29
static void capsense_touch_task(void *p);
void cap_touch_IRQ(void);

int main(void) {
  i2c2_pin_config();
  i2c__initialize(I2C__2, 100000, 96000000);
  initialize_display();
  for (int i = 0; i < 12; i++) {
    square_anim_start[i] = xSemaphoreCreateBinary();
    square_anim_touch[i] = xSemaphoreCreateBinary();
    xTaskCreate(square_anim_task, "squares", 2000 / sizeof(void *), (void *)i, PRIORITY_LOW, NULL);
  }
  game_start = xSemaphoreCreateBinary();
  start_beat = xSemaphoreCreateBinary();
  button_events = xEventGroupCreate();
  xTaskCreate(game_loop_task, "game_loop", 8000 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(display_score_task, "score", 4000 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(freq_task, "freq", 4000 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  update_score = xSemaphoreCreateBinary();
  cap_touch_detected = xSemaphoreCreateBinary();
  gpio__attach_interrupt(6, 2, GPIO_INTR__FALLING_EDGE, cap_touch_IRQ);
  lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__GPIO, gpio__interrupt_dispatcher, NULL);
  xTaskCreate(capsense_touch_task, "capsense", 2000 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  // active_event_mutex = xSemaphoreCreateMutex();
  vTaskStartScheduler(); // This function never returns unless RTOS scheduler runs out of memory and fails

  return 0;
}
/* TASKS */

static void animate(uint8_t frame, position top_left, position bottom_right) {
  if (frame < 22) {
    draw_animation(top_left, frame, 1);
  } else {
    clear_rectangle(top_left, bottom_right);
    draw_animation(top_left, frame, 4);
  }
}
static void square_anim_task(void *param) {
  const uint8_t button = (uint8_t)param;
  const uint8_t row = button / 4 + 1;
  const uint8_t col = button % 4;
  const position top_left = {16 * col + 1, 16 * row + 1};
  const position bottom_right = {16 * col + 14, 16 * row + 14};
  uint8_t count = 0;
  // fill_rectangle(top_left, bottom_right, 1);
  // while (1) {
  //   xSemaphoreTake(square_anim_start[button], portMAX_DELAY);
  //   fill_rectangle(top_left, bottom_right, 1);
  //   vTaskDelay(100);
  //   clear_rectangle(top_left, bottom_right);
  // }
  while (1) {
    if (xSemaphoreTake(square_anim_start[button], portMAX_DELAY)) {
      draw_animation(top_left, 0, 1);
      for (uint8_t i = 1; i < 32; i++) {
        if (xSemaphoreTake(square_anim_touch[button], 29)) {
          // Touched
          // White if perfect, yellow if not
          uint8_t color = 6;
          if (isPerfect(button)) {
            color = 7;
          }
          fill_rectangle(top_left, bottom_right, color);
          vTaskDelay(100);
          clear_rectangle(top_left, bottom_right);
          break;
        } else {
          animate(i, top_left, bottom_right);
        }
      }
      clear_rectangle(top_left, bottom_right);
    }
  }
}

static void game_loop_task(void *param) {
  // Variabale initialization
  uint32_t event_time = 0;
  uint8_t button = 0;
  uint16_t event_count = 0;
  game_state = 6;
  draw_string((position){15, 5}, "LOADING", 5);
  getSongInfo(1);
  // Delay to wait for audio player to start up
  setup_audio();
  audio_reset(UART__3);
  vTaskDelay(4000);
  // GAME STARTS HERE
  clear_rectangle((position){0, 0}, (position){63, 63});
  while (1) {
    // START MENU HERE
    clear_rectangle((position){15, 0}, (position){63, 63});
    start_menu();
    xSemaphoreTake(game_start, portMAX_DELAY);
    game_state = 0;
    transition(7);
    bool start_playing = false;
    draw_string((position){2, 0}, "SELECT", 6);
    draw_string((position){25, 7}, "SONG", 6);
    xEventGroupSetBits(button_events, 1);
    while (1) {
      if (xEventGroupWaitBits(button_events, 0b111111111111, pdFALSE, pdFALSE, portMAX_DELAY)) {
        switch (xEventGroupGetBits(button_events)) {
        case (1 << 11):
          start_playing = true;
          break; // Start
        case (1 << 8):
          getSongInfo(song_number - 1);
          break;
        case (1 << 9):
          getSongInfo(song_number + 1);
          break;
        }
        clear_rectangle((position){0, 16}, (position){63, 31});
        clear_rectangle((position){2, 39}, (position){63, 46});
        draw_string((position){1, 17}, song_name, 2);
        draw_left_arrow((position){3, 56}, 4);
        draw_right_arrow((position){29, 56}, 1);
        draw_go_button((position){49, 55}, 3, 5);
        draw_string((position){2, 32}, "HIGHSCORE", 7);
        int highscore;
        char scr[6];
        get_score(&highscore);
        sprintf(scr, "%d", highscore);
        draw_string((position){2, 39}, scr, 7);
        if (start_playing) {
          break;
        }
        xEventGroupClearBits(button_events, 0b111111111111);
      }
    }
    // SONG SETUP
    game_state = 4;
    gameplay_countdown();
    // Initial song setup
    FIL song_data_file = open_song_data(song_file);
    get_next_song_line(&event_time, &button, &song_data_file);
    audio_init(UART__3, 30);
    audio_specify_track(UART__3, song_number);
    audio_start_play(UART__3);
    xSemaphoreGive(start_beat);
    xSemaphoreGive(update_score);
    game_state = 1;
    uint32_t startTick = xTaskGetTickCount();
    while (1) {
      song_time = xTaskGetTickCount() - startTick;
      // display event!
      if (event_time - 609 <= song_time) {
        xSemaphoreGive(square_anim_start[button]);
        event_count++;
        load_event(event_time, button);
        if (!get_next_song_line(&event_time, &button, &song_data_file)) {
          // THE END
          for (int i = 0; i < 500; i++) {
            song_time = xTaskGetTickCount() - startTick;
            vTaskDelay(10);
          }
          game_state = 4;
          end_screen(event_count);
          xSemaphoreTake(game_start, 0);
          capsense_reset();
          game_state = 2;
          break;
        }
      } else {
        vTaskDelay(10);
      }
    }
    xSemaphoreTake(game_start, portMAX_DELAY);
    transition(6);
    score = 0;
    game_state = 6;
    event_count = 0;
  }
}

void capsense_reset() {
  i2c__write_single(I2C__2, 0xB4, 0x80, 0x63); // soft_reset has sth to do with the poer on set
  i2c__write_single(I2C__2, 0xB4, 0x5E, 0x0);  // set to run mode & enable pin 0.
  uint8_t ECR_val = i2c__read_single(I2C__2, 0xB4, 0x5D);
  if (ECR_val != 0x24)
    fprintf(stderr, "false false false 0x5E not 24 %x", ECR_val);

  // sets threshold values for touch & release
  for (uint8_t i = 0; i < 12; i++) {
    i2c__write_single(I2C__2, 0xB4, MPR121_TOUCHTH_0reg + 2 * i, touch_TH_val);
    i2c__write_single(I2C__2, 0xB4, MPR121_RELEASETH_0reg + 2 * i, release_TH_val);
  }

  /* initally the sensor is in stop mode. Set it to run mode after setting all other configurations** (can only be set
   * during stop mode ) */
  uint8_t ECR_SETTING = 0b10000000 + 0b1100;          // 5 bits for baseline tracking & proximity disabled + X
                                                      // x = amount of electrodes running (12 or 0b1100 for all)
  i2c__write_single(I2C__2, 0xB4, 0x5E, ECR_SETTING); // seting ECR register at the end to change it to run mode
}
static void capsense_touch_task(void *p) {
  uint16_t last_touched = 0;
  uint16_t currently_touch = 0;
  capsense_reset();
  while (1) {

    xSemaphoreTake(cap_touch_detected, portMAX_DELAY);
    currently_touch = (i2c__read_single(I2C__2, 0xB4, 0x01) << 8); // the MSB 4 input pins (pin 11 - 8)
    currently_touch += i2c__read_single(I2C__2, 0xB4, 0x00);       // add the LSB 8 input pins (pin 7 - 0)
    currently_touch &= 0x0FFF;                                     // remove 4 MSB for slave register 0x01

    for (uint8_t i = 0; i < 12; i++) {
      // if the pin is getting touch now. (2nd variable checks if it is just a long touch, if yes, eliminate)
      if ((currently_touch & (1 << i)) && !(last_touched & (1 << i))) {
        touched(i);
      }
      // if the pin is not being touched now but it has been touched previously.
      if (!(currently_touch & (1 << i)) && (last_touched & (1 << i))) {
      }
    }

    last_touched = currently_touch;
  }
}
void start_menu() {
  uint8_t color = 1;
  if (game_state != 2) {
    draw_string((position){15, 35}, "JUBEAT", color);
    vTaskDelay(100);
    for (uint8_t x = 35; x > 3; x -= 1) {
      clear_rectangle((position){0, x + 1}, (position){63, x + 8});
      draw_string((position){15, x}, "JUBEAT", color);
      vTaskDelay(50);
      color = (color + 1) % 8 + 1;
    }
  }
  draw_string((position){18, 35}, "START", 2);
}

void gameplay_countdown(void) {
  draw_64x64_frame_animation(6, 4);
  // draw_letter((position){21, 19}, '3', 3);
  vTaskDelay(1000);
  clear_rectangle((position){0, 0}, (position){63, 63});
  // clear_rectangle((position){17, 17}, (position){30, 30});

  draw_64x64_frame_animation(12, 6);
  // draw_letter((position){37, 35}, '2', 4);
  vTaskDelay(1000);
  clear_rectangle((position){0, 0}, (position){63, 63});
  // clear_rectangle((position){33, 33}, (position){46, 46});

  draw_64x64_frame_animation(16, 2);
  // draw_letter((position){21, 35}, '1', 5);
  vTaskDelay(1000);
  // clear_rectangle((position){17, 33}, (position){30, 46});

  // draw_64x64_frame_animation(16, 6);
  // draw_string((position){37, 19}, "0", 6);
  // vTaskDelay(1000);
  // clear_rectangle((position){33, 17}, (position){46, 30});

  draw_string((position){20, 20}, "GAME", 7);
  draw_string((position){18, 35}, "START", 7);
  vTaskDelay(500);
  transition(1);
  // clear_rectangle((position){0, 0}, (position){63, 63});
}

void touched(uint8_t button) {
  switch (game_state) {
  case 0: // Menu
    // xSemaphoreGive(square_anim_start[button]);

    xEventGroupSetBits(button_events, (1 << button));
    break;
  case 1: // Game
    compare_touch_to_event(button);
    break;
  case 2: // End
    xSemaphoreGive(game_start);
    break;
  case 6:
    xSemaphoreGive(game_start);
    break;
  }
}
/* FUNCTIONS AND HELPER FUNCTIONS AND THINGS OF THAT NATURE */

void cap_touch_IRQ(void) {
  xSemaphoreGiveFromISR(cap_touch_detected, NULL);

  clear_pin_interrupt(6, 2);
}

void setup_audio() {
  uart__init(UART__3, 96000000, 9600);
  gpio__construct_with_function(4, 28, GPIO__FUNCTION_2);
  gpio__construct_with_function(4, 29, GPIO__FUNCTION_2);
}

FIL open_song_data(char *filename) {
  FIL file;
  f_open(&file, filename, (FA_READ));
  return file;
}

bool get_next_song_line(uint32_t *time, uint8_t *button, FIL *file) {
  TCHAR line[11];
  TCHAR *data = f_gets(line, sizeof(line), file);
  if (data == 0) {
    return false;
  }
  uint32_t num = atoi(data);
  *button = num % 100;
  *time = num / 100;
  return true;
}

void load_event(uint32_t time, uint8_t button) { active_events[button] = time; }

void compare_touch_to_event(uint8_t button) {
  int time_difference = (int)active_events[button] - (int)song_time;
  if ((time_difference < 300) && (time_difference > -50)) {
    timing += time_difference;
    xSemaphoreGive(square_anim_touch[button]);
    if (isPerfect(button)) {
      score += 2;
    } else {
      score += 1;
    }
    xSemaphoreGive(update_score);
  }
  fprintf(stderr, "Button: %u, time difference: %d", button, time_difference);
}
static void i2c2_pin_config(void) {
  uint32_t function_2 = (0b010 << 0);
  uint32_t mode_inactive = (0b11 << 3);
  uint32_t open_drain_mode = (1 << 10);

  LPC_IOCON->P0_10 &= ~(0b111 << 0); // reset func bits for P0.10 I2C2-SDA
  LPC_IOCON->P0_11 &= ~(0b111 << 0); // reset func bits for P0.11 I2C2-SCL
  LPC_IOCON->P0_10 |= function_2;
  LPC_IOCON->P0_11 |= function_2;

  LPC_IOCON->P0_10 &= ~mode_inactive; // reset mode bits.
  LPC_IOCON->P0_11 &= ~mode_inactive;
  LPC_IOCON->P0_10 |= open_drain_mode; // set to OD mode
  LPC_IOCON->P0_11 |= open_drain_mode;
}

static void display_score_task(void *param) {
  char scr[10];

  while (1) {
    if (game_state == 1) {
      draw_string((position){3, 5}, "SCORE ", 7);
      if (xSemaphoreTake(update_score, portMAX_DELAY)) {
        clear_rectangle((position){35, 5}, (position){64, 16});
        sprintf(scr, " %d", score);
        draw_string((position){35, 5}, scr, 7);
      }
    }
    vTaskDelay(100);
  }
}

bool get_next_freq_line(uint8_t *value, FIL *file) {
  TCHAR line[3];
  TCHAR *data = f_gets(line, sizeof(line), file);
  if (data == 0) {
    return false;
  }
  *value = atoi(data);
  return true;
}

static void freq_task(void *param) {
  position a = {0, 0};
  uint8_t freq = 0, color = 4;
  TickType_t xLastWakeTime;
  const TickType_t xFrequency = 30;
  // int count = 0;
  while (1) {
    xSemaphoreTake(start_beat, portMAX_DELAY);
    xLastWakeTime = xTaskGetTickCount();
    FIL song_freq_file = open_song_data(freq_file);
    while (1) {
      if (get_next_freq_line(&freq, &song_freq_file)) {
        clear_rectangle(a, (position){63, 3});
        fill_rectangle(a, (position){freq, 3}, color);
        color = (rand() % 7) + 1;
        if (game_state != 1) {
          clear_rectangle(a, (position){63, 3});
          break;
        }
        // clear_rectangle(a, (position){63, 16});
        // char scr[10];
        // sprintf(scr, " %d", count);
        // draw_string(a, scr, 1);
        // count++;
        vTaskDelayUntil(&xLastWakeTime, xFrequency);
      } else {
        clear_rectangle(a, (position){63, 3});
        break;
      }
    }
  }
}

/* screen after song is over, show final score? grade? */
void end_screen(uint16_t event_count) {
  draw_string((position){16, 33}, "The End", 6);
  int count = 0;
  int color = 4;
  int max_score = event_count * 2;
  clear_rectangle((position){0, 0}, (position){63, 15});
  position a = {5, 20};
  position b = {16 + 5, 20};
  position c = {16 * 2 + 5, 20};
  position d = {16 * 3 + 5, 20};
  while (count <= score) {

    int fill_area = (count * 64) / max_score;
    if (fill_area < 16) {
      color = 4;
    } else if (fill_area < 32) {
      color = 6;
    } else if (fill_area < 48) {
      color = 2;
    } else if (fill_area < 56) {
      color = 3;
    } else {
      color = 7;
    }
    fill_rectangle((position){0, 0}, (position){fill_area, 15}, color);
    char score_a = ((count / 1000) % 10) + '0';
    char score_b = ((count / 100) % 10) + '0';
    char score_c = ((count / 10) % 10) + '0';
    char score_d = (count % 10) + '0';
    clear_rectangle((position){0, 16}, (position){63, 31});
    draw_letter(a, score_a, 1);
    draw_letter(b, score_b, 1);
    draw_letter(c, score_c, 1);
    draw_letter(d, score_d, 1);
    count++;
    vTaskDelay(20);
  }
  char scr[10];
  int highscore;

  // sprintf(scr, " %d", timing / event_count);
  // draw_string((position){0, 48}, scr, 7);
  get_score(&highscore);
  draw_string((position){37, 39}, "HIGH", 7);
  if (score > highscore) {
    save_score(score);
    for (int sc = highscore; sc <= score; sc++) {
      clear_rectangle((position){38, 49}, (position){63, 55});
      sprintf(scr, " %d", sc);
      draw_string((position){38, 49}, scr, 7);
      vTaskDelay(20);
    }
  } else {
    clear_rectangle((position){38, 49}, (position){63, 55});
    sprintf(scr, " %d", highscore);
    draw_string((position){38, 49}, scr, 4);
  }

  volume_fade_out();
}

bool isPerfect(uint8_t button) {
  int time_difference = abs(active_events[button] - song_time);
  if (time_difference < 60) {
    return true;
  } else {
    return false;
  }
}

void getSongInfo(uint8_t selection) {
  FIL file;
  f_open(&file, "list", (FA_READ));
  if (selection == 0) {
    return;
  }
  TCHAR *data;
  for (int i = 0; i < selection * 3; i++) {
    switch (i % 3) {
    case 0:
      data = f_gets(song_name, sizeof(song_name), &file);
      break;
    case 1:
      data = f_gets(song_file, sizeof(song_file), &file);
      break;
    case 2:
      data = f_gets(freq_file, sizeof(freq_file), &file);
      break;
    }
  }
  if (data == 0) {
    getSongInfo(selection - 1);
    return;
  }
  song_number = selection;
}

void save_score(int score_h) {

  char scr[6];
  FIL fil;
  f_open(&fil, song_name, FA_OPEN_ALWAYS | FA_WRITE);
  sprintf(scr, " %d", score_h);
  f_puts(scr, &fil);
  f_close(&fil);
}

void get_score(int *score_h) {
  char scr[6];
  FIL fil;
  f_open(&fil, song_name, FA_OPEN_ALWAYS | FA_READ);
  f_gets(scr, 6, &fil);
  *score_h = atoi(scr);
  f_close(&fil);
}

void transition(uint8_t color) {
  uint8_t size = 1;
  for (int i = 31; i >= 1; i--) {
    for (int j = 0; j < size; j++) {
      draw_pixel((position){i + j, i}, color);
      draw_pixel((position){i + j, i + size}, color);
      draw_pixel((position){i, i + j}, color);
      draw_pixel((position){i + size, i + j}, color);
    }
    // fill_rectangle((position){i, i}, (position){i + size, i + size}, color);
    clear_rectangle((position){i + 1, i + 1}, (position){i + size - 1, i + size - 1});
    size += 2;
    vTaskDelay(20);
  }
  clear_rectangle((position){0, 0}, (position){63, 63});
}

void draw_go_button(position p, uint8_t frame_color, uint8_t go_color) {
  for (int j = -3; j < 6; j++) {
    draw_pixel((position){p.x, p.y + j}, frame_color);      // draw left column
    draw_pixel((position){p.x + 13, p.y + j}, frame_color); // draw right column
  }
  for (int i = 2; i < 12; i++) {
    draw_pixel((position){p.x + i, p.y - 5}, frame_color); // draw top row
    draw_pixel((position){p.x + i, p.y + 7}, frame_color); // draw bottom row
  }
  draw_pixel((position){p.x + 1, p.y - 4}, frame_color);
  draw_pixel((position){p.x + 12, p.y - 4}, frame_color);
  draw_pixel((position){p.x + 1, p.y + 6}, frame_color);
  draw_pixel((position){p.x + 12, p.y + 6}, frame_color);
  draw_string((position){p.x + 1, p.y - 3}, "GO", go_color);
  draw_pixel((position){p.x + 4, p.y + 1}, go_color);
}

void volume_fade_out() {
  uint8_t current_vol = volume;
  for (int i = current_vol; i >= 0; i--) {
    audio_set_volume(UART__3, current_vol--);
    vTaskDelay(100);
  }
  audio_reset(UART__3);
  audio_set_volume(UART__3, volume);
}
