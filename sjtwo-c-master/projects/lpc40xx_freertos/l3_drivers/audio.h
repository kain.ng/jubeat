/* This code supports the external audio module: DFPlayer mini */
#ifndef audio_h
#define audio_h
#include "uart.h"
#include <stdio.h>

extern const uint8_t start_byte;
extern const uint8_t version_byte;
extern const uint8_t command_length;
extern const uint8_t feedback; // no feedback return; use 0x01 for feedback
extern const uint8_t end_byte;

// void send_audio_command(uart_e uart, uint8_t audio_command, uint8_t param1, uint8_t param2);
void audio_init(uart_e uart, uint8_t volume);
void audio_set_volume(uart_e uart, uint8_t volume);

void audio_start_play(uart_e uart);
void audio_specify_track(uart_e uart, uint8_t track_number);
void audio_reset(uart_e uart);
#endif