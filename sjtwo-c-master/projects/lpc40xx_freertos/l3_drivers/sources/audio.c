#include "audio.h"
#include "FreeRTOS.h"
#include "delay.h"
#include "uart.h"
#include <stdio.h>

/*
This driver is developed for Jubeat (music game) using the DFPlayer mp3 module
1. choice of "repeated mode" is not available yet
2. This driver only supports SD cards. USB or flash is not supported yet.
3. This driver assumes all songs, each named in a number (ie. 001.mp3), are stored in a folder named "01" in the SD
card.
4. Up to 256 songs can be stored and played using this driver. Support for 256+ songs is not available yet
*/

const uint8_t start_byte = 0x7E;
const uint8_t version_byte = 0xFF;
const uint8_t command_length = 0x06;
const uint8_t feedback = 0x00; // no feedback return; use 0x01 for feedback
const uint8_t end_byte = 0xEF;

uint16_t audio_calculate_2byte_checksum(uint8_t audio_command, uint8_t param1, uint8_t param2) {
  uint16_t checksum = (version_byte + command_length + audio_command + feedback + param1 + param2);
  return -checksum;
}

void send_audio_command(uart_e uart, uint8_t audio_command, uint8_t param1, uint8_t param2) {
  uint16_t checksum_2byte = audio_calculate_2byte_checksum(audio_command, param1, param2);
  uint8_t checksum_lowbyte = checksum_2byte;
  uint8_t checksum_highbyte = (checksum_2byte >> 8);

  uint8_t audio_full_command[10] = {start_byte, version_byte, command_length,    audio_command,    feedback,
                                    param1,     param2,       checksum_highbyte, checksum_lowbyte, end_byte};
  for (int i = 0; i < 10; i++) {
    uart__polled_put(uart, audio_full_command[i]);
  }
}

// songs are not played until calling audio_specify_track & audio_start_play
void audio_init(uart_e uart, uint8_t volume) {
  send_audio_command(uart, 0x0E, 0x00,
                     0x00); // Pause playback, so we don't have to reset it manually through everyre-flash
  send_audio_command(uart, 0x09, 0x00, 0x02); // specify playback device to micro SD card
  send_audio_command(uart, 0x3F, 0x00, 0x00); // init param
  audio_set_volume(uart, volume);
}

/* Enter value of volume from 0x00 to 0x1E (30 level of adjustment) */
void audio_set_volume(uart_e uart, uint8_t volume) {
  // set volume after init
  send_audio_command(uart, 0x06, 0x00, volume);
}

/* Name your songs in numbers between 0x01 to 0xFF */
void audio_specify_track(uart_e uart, uint8_t track_number) { send_audio_command(uart, 0x0F, 0x01, track_number); }

void audio_start_play(uart_e uart) { send_audio_command(uart, 0x0D, 0x00, 0x00); }

void audio_reset(uart_e uart) { send_audio_command(uart, 0x0E, 0x00, 0x00); }