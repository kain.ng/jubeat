#include "gpio_int.h"

function_pointer_t gpio_int_lookup[32][3];

// enable interrupt
void enable_pin_interrupt(uint8_t pin, uint8_t port, gpio_interrupt_e int_type);

uint8_t get_pin_port(uint8_t *port) {
  uint8_t bit;

  uint32_t reg;
  if (LPC_GPIOINT->IO0IntStatF) {
    *port = 0;
    bit = 0;
    reg = LPC_GPIOINT->IO0IntStatF;
    while (1) {
      reg = reg >> 1;
      if (reg == 0) {

        return bit;
      }
      ++bit;
    }
  } else if (LPC_GPIOINT->IO2IntStatF) {
    *port = 2;
    bit = 0;
    reg = LPC_GPIOINT->IO2IntStatF;
    while (1) {
      reg = reg >> 1;
      if (reg == 0) {

        return bit;
      }
      ++bit;
    }
  } else if (LPC_GPIOINT->IO0IntStatR) {
    *port = 0;
    bit = 0;
    reg = LPC_GPIOINT->IO0IntStatR;
    while (1) {
      reg = reg >> 1;
      if (reg == 0) {

        return bit;
      }
      ++bit;
    }
  } else if (LPC_GPIOINT->IO2IntStatR) {
    *port = 2;
    bit = 0;
    reg = LPC_GPIOINT->IO2IntStatR;
    while (1) {
      reg = reg >> 1;
      if (reg == 0) {

        return bit;
      }
      ++bit;
    }
  }
  return 255; // error
}

void gpio__attach_interrupt(uint8_t pin, uint8_t port, gpio_interrupt_e int_type, function_pointer_t callback) {

  enable_pin_interrupt(pin, port, int_type);

  gpio_int_lookup[pin][port] = callback;
}

void gpio__interrupt_dispatcher(void) {
  uint8_t port;

  uint8_t pin_that_generated_interrupt = get_pin_port(&port);
  function_pointer_t attached_user_handler = gpio_int_lookup[pin_that_generated_interrupt][port];
  attached_user_handler();
}

void clear_pin_interrupt(uint8_t pin, uint8_t port) {
  if (port == 0) {
    LPC_GPIOINT->IO0IntClr = (1 << pin);
  } else if (port == 2) {
    LPC_GPIOINT->IO2IntClr = (1 << pin);
  }
}

void enable_pin_interrupt(uint8_t pin, uint8_t port, gpio_interrupt_e int_type) {
  if (port == 0) {
    if (int_type == GPIO_INTR__FALLING_EDGE) {
      LPC_GPIOINT->IO0IntEnF |= (1 << pin);
    } else if (int_type == GPIO_INTR__RISING_EDGE) {
      LPC_GPIOINT->IO0IntEnR |= (1 << pin);
    }
  } else if (port == 2) {
    if (int_type == GPIO_INTR__FALLING_EDGE) {
      LPC_GPIOINT->IO2IntEnF |= (1 << pin);
    } else if (int_type == GPIO_INTR__RISING_EDGE) {
      LPC_GPIOINT->IO2IntEnR |= (1 << pin);
    }
  }
}