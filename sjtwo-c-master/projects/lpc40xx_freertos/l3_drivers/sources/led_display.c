
#include "led_display.h"
#include "FreeRTOS.h"
#include "HISKYF21_font.h"

#include "animation.h"
#include "delay.h"
#include "gpio.h"
#include "stdio.h"
#include "task.h"

static volatile uint32_t led_matrix[3][64][2];
static void display_task_gpio(void *param);

//#define DFROBOT

#ifdef DFROBOT
void Write_REG1(gpio_s A, gpio_s B, gpio_s C, gpio_s D, gpio_s E, gpio_s F, gpio_s CLK, gpio_s LAT,
                unsigned int REG_DATA) {
  gpio__reset(CLK);
  gpio__reset(LAT);

  for (int i = 0; i < 11; i++) {
    int DIN = REG_DATA;
    for (int j = 0; j < 16; j++) {
      if (DIN & 0x8000) {
        gpio__set(A);
        gpio__set(B);
        gpio__set(C);
        gpio__set(D);
        gpio__set(E);
        gpio__set(F);
      } else {
        gpio__reset(A);
        gpio__reset(B);
        gpio__reset(C);
        gpio__reset(D);
        gpio__reset(E);
        gpio__reset(F);
      }
      gpio__reset(CLK);
      gpio__set(CLK);
      DIN = DIN << 1;
    }
  }

  int DIN = REG_DATA;

  for (int i = 0; i < 5; i++) {
    if (DIN & 0x8000) {
      gpio__set(A);
      gpio__set(B);
      gpio__set(C);
      gpio__set(D);
      gpio__set(E);
      gpio__set(F);
    } else {
      gpio__reset(A);
      gpio__reset(B);
      gpio__reset(C);
      gpio__reset(D);
      gpio__reset(E);
      gpio__reset(F);
    }

    gpio__reset(CLK);
    gpio__set(CLK);
    DIN = DIN << 1;
  }

  gpio__set(LAT);

  for (int i = 0; i < 11; i++) {
    if (DIN & 0x8000) {
      gpio__set(A);
      gpio__set(B);
      gpio__set(C);
      gpio__set(D);
      gpio__set(E);
      gpio__set(F);
    } else {
      gpio__reset(A);
      gpio__reset(B);
      gpio__reset(C);
      gpio__reset(D);
      gpio__reset(E);
      gpio__reset(F);
    }

    gpio__reset(CLK);
    gpio__set(CLK);
    DIN = DIN << 1;
  }
  gpio__reset(CLK);
  gpio__reset(LAT);
}

void Write_REG2(gpio_s A, gpio_s B, gpio_s C, gpio_s D, gpio_s E, gpio_s F, gpio_s CLK, gpio_s LAT,
                unsigned int REG_DATA) {
  gpio__reset(CLK);
  gpio__reset(LAT);

  for (int i = 0; i < 11; i++) {
    int DIN = REG_DATA;
    for (int j = 0; j < 16; j++) {
      if (DIN & 0x8000) {
        gpio__set(A);
        gpio__set(B);
        gpio__set(C);
        gpio__set(D);
        gpio__set(E);
        gpio__set(F);
      } else {
        gpio__reset(A);
        gpio__reset(B);
        gpio__reset(C);
        gpio__reset(D);
        gpio__reset(E);
        gpio__reset(F);
      }
      gpio__reset(CLK);
      gpio__set(CLK);
      DIN = DIN << 1;
    }
  }

  int DIN = REG_DATA;

  for (int i = 0; i < 4; i++) {
    if (DIN & 0x8000) {
      gpio__set(A);
      gpio__set(B);
      gpio__set(C);
      gpio__set(D);
      gpio__set(E);
      gpio__set(F);
    } else {
      gpio__reset(A);
      gpio__reset(B);
      gpio__reset(C);
      gpio__reset(D);
      gpio__reset(E);
      gpio__reset(F);
    }

    gpio__reset(CLK);
    gpio__set(CLK);
    DIN = DIN << 1;
  }

  gpio__set(LAT);

  for (int i = 0; i < 12; i++) {
    if (DIN & 0x8000) {
      gpio__set(A);
      gpio__set(B);
      gpio__set(C);
      gpio__set(D);
      gpio__set(E);
      gpio__set(F);
    } else {
      gpio__reset(A);
      gpio__reset(B);
      gpio__reset(C);
      gpio__reset(D);
      gpio__reset(E);
      gpio__reset(F);
    }

    gpio__reset(CLK);
    gpio__set(CLK);
    DIN = DIN << 1;
  }
  gpio__reset(CLK);
  gpio__reset(LAT);
}
#endif

void draw_pixel(position p, uint8_t color_8) {

  if (p.x < 32) {
    // clearing previous COLOR
    (led_matrix[0][p.y][0]) &= ~((uint32_t)(1 << (31 - p.x)));
    (led_matrix[1][p.y][0]) &= ~((uint32_t)(1 << (31 - p.x)));
    (led_matrix[2][p.y][0]) &= ~((uint32_t)(1 << (31 - p.x)));
    // NEW COLOR
    (led_matrix[0][p.y][0]) |= (((uint32_t)(color_8 & 0x4) >> 2) << (31 - p.x));
    (led_matrix[1][p.y][0]) |= (((uint32_t)(color_8 & 0x2) >> 1) << (31 - p.x));
    (led_matrix[2][p.y][0]) |= (((uint32_t)(color_8 & 0x1) >> 0) << (31 - p.x));
  } else {
    // clearing previous COLOR
    (led_matrix[0][p.y][1]) &= ~((uint32_t)(1 << (31 - (p.x - 32))));
    (led_matrix[1][p.y][1]) &= ~((uint32_t)(1 << (31 - (p.x - 32))));
    (led_matrix[2][p.y][1]) &= ~((uint32_t)(1 << (31 - (p.x - 32))));
    // NEW COLOR
    (led_matrix[0][p.y][1]) |= (((uint32_t)(color_8 & 0x4) >> 2) << (31 - (p.x - 32)));
    (led_matrix[1][p.y][1]) |= (((uint32_t)(color_8 & 0x2) >> 1) << (31 - (p.x - 32)));
    (led_matrix[2][p.y][1]) |= (((uint32_t)(color_8 & 0x1) >> 0) << (31 - (p.x - 32)));
  }
}

static void display_task_gpio(void *param) {
  uint32_t logic;
  const gpio_s A = {1, 20}, B = {1, 23}, C = {1, 28}, D = {1, 29}, E = {1, 30}, OE = {2, 2}, LAT = {2, 1};
  const gpio_s R1 = {0, 6}, G1 = {0, 7}, B1 = {0, 8}, CLK = {2, 0}, R2 = {0, 26}, G2 = {0, 25}, B2 = {1, 31};

  gpio__set_as_output(A);
  gpio__set_as_output(B);
  gpio__set_as_output(C);
  gpio__set_as_output(D);
  gpio__set_as_output(E);
  gpio__set_as_output(OE);
  gpio__set_as_output(LAT);
  gpio__set_as_output(R1);
  gpio__set_as_output(G1);
  gpio__set_as_output(B1);
  gpio__set_as_output(R2);
  gpio__set_as_output(G2);
  gpio__set_as_output(B2);
  gpio__set_as_output(CLK);
#ifdef DFROBOT
  Write_REG1(R1, G1, B1, R2, G2, B2, CLK, LAT, 0xFFC2);
  Write_REG2(R1, G1, B1, R2, G2, B2, CLK, LAT, 0x6862);
#endif
  gpio__reset(CLK);
  gpio__reset(OE);
  TickType_t xLastWakeTime;
  const TickType_t xFrequency = 10;

  xLastWakeTime = xTaskGetTickCount();
  while (1) {
    // gpio__set(OE);
    for (int row = 0; row < 32; row++) {
      // left half
      for (int x = 31; x >= 0; x--) {
        logic = ((led_matrix[0][row][0] >> x) & 1);
        gpio__set_logic(R1, logic);

        logic = ((led_matrix[1][row][0] >> x) & 1);
        gpio__set_logic(G1, logic);

        logic = ((led_matrix[2][row][0] >> x) & 1);
        gpio__set_logic(B1, logic);

        logic = ((led_matrix[0][row + 32][0] >> x) & 1);
        gpio__set_logic(R2, logic);
        logic = ((led_matrix[1][row + 32][0] >> x) & 1);
        gpio__set_logic(G2, logic);
        logic = ((led_matrix[2][row + 32][0] >> x) & 1);
        gpio__set_logic(B2, logic);
        gpio__set(CLK);

        gpio__reset(CLK);
      }
      // right half
      for (int x = 31; x >= 0; x--) {
        logic = ((led_matrix[0][row][1] >> x) & 1);
        gpio__set_logic(R1, logic);

        logic = ((led_matrix[1][row][1] >> x) & 1);
        gpio__set_logic(G1, logic);

        logic = ((led_matrix[2][row][1] >> x) & 1);
        gpio__set_logic(B1, logic);

        logic = ((led_matrix[0][row + 32][1] >> x) & 1);
        gpio__set_logic(R2, logic);
        logic = ((led_matrix[1][row + 32][1] >> x) & 1);
        gpio__set_logic(G2, logic);
        logic = ((led_matrix[2][row + 32][1] >> x) & 1);
        gpio__set_logic(B2, logic);
        gpio__set(CLK);

        gpio__reset(CLK);
      }
      gpio__set(OE);
      gpio__set(LAT);
      gpio__set_logic(A, (row & (1 << 0)) >> 0);
      gpio__set_logic(B, (row & (1 << 1)) >> 1);
      gpio__set_logic(C, (row & (1 << 2)) >> 2);
      gpio__set_logic(D, (row & (1 << 3)) >> 3);
      gpio__set_logic(E, (row & (1 << 4)) >> 4);
      gpio__reset(LAT);
      gpio__reset(OE);
    }
    vTaskDelayUntil(&xLastWakeTime, xFrequency);
  }
}

void draw_letter(position p, char character, uint8_t color) {
  character = character & 0x7F;
  if (character < ' ') {
    character = 0;
  } else {
    character -= ' ';
    // character *= 6;
  }
  const uint8_t *ch = font[character];
  for (int i = 0; i < 6; i++) {
    for (int j = 0; j < 8; j++) {
      if (ch[i] & (1 << j))
        draw_pixel((position){i + p.x, j + p.y}, color);
    }
  }
}

void draw_animation(position p, uint8_t frame, uint8_t color) {
  const uint16_t *ch = animation[frame];
  for (int i = 0; i < 14; i++) {
    for (int j = 2; j < 16; j++) {
      if (ch[i] & (1 << j))
        draw_pixel((position){i + p.x, j + p.y - 2}, color);
    }
  }
}

void draw_64x64_frame_animation(uint8_t border_thickness, uint8_t color) {
  for (int i = 0; i < 64; i += 2) {
    for (int j = 0; j < border_thickness; j++) {
      draw_pixel((position){i, j}, color);
    }
    for (int j = (64 - border_thickness); j < 64; j++) {
      draw_pixel((position){i, j}, color);
    }
  }

  for (int j = border_thickness; j < (64 - border_thickness); j += 2) {
    for (int i = 0; i < border_thickness; i++) {
      draw_pixel((position){i, j}, color);
    }
    for (int i = (64 - border_thickness); i < 64; i++) {
      draw_pixel((position){i, j}, color);
    }
  }
}

void draw_string(position p, char *string, uint8_t color) {
  int i = 0, j = 0;
  while (string[i] != '\0') {
    if (string[i] == '\n') {
      p.y = p.y + 8;
      p.x = 0;
      j = -6;
    }

    draw_letter((position){p.x + j, p.y}, string[i], color);
    j += 6;
    i += 1;
  }
}

void draw_left_arrow(position tip, uint8_t color) {
  draw_pixel(tip, color); // arrow tip

  for (int i = 1; i < 10; i++) {
    for (int j = -1; j < 2; j++) {
      draw_pixel((position){tip.x + i, tip.y + j}, color);
    }
  }

  draw_pixel((position){tip.x + 2, tip.y - 2}, color);
  draw_pixel((position){tip.x + 3, tip.y - 2}, color);
  draw_pixel((position){tip.x + 3, tip.y - 3}, color);

  draw_pixel((position){tip.x + 2, tip.y + 2}, color);
  draw_pixel((position){tip.x + 3, tip.y + 2}, color);
  draw_pixel((position){tip.x + 3, tip.y + 3}, color);
}

void draw_right_arrow(position tip, uint8_t color) {
  draw_pixel(tip, color); // arrow tip

  for (int i = 1; i < 10; i++) {
    for (int j = -1; j < 2; j++) {
      draw_pixel((position){tip.x - i, tip.y + j}, color);
    }
  }

  draw_pixel((position){tip.x - 2, tip.y - 2}, color);
  draw_pixel((position){tip.x - 3, tip.y - 2}, color);
  draw_pixel((position){tip.x - 3, tip.y - 3}, color);

  draw_pixel((position){tip.x - 2, tip.y + 2}, color);
  draw_pixel((position){tip.x - 3, tip.y + 2}, color);
  draw_pixel((position){tip.x - 3, tip.y + 3}, color);
}

void clear_rectangle(position adj1, position adj2) {
  uint8_t i, j;
  for (i = adj1.x; i <= adj2.x; i++) {
    for (j = adj1.y; j <= adj2.y; j++) {
      if (i < 32) {
        // clearing previous COLOR
        (led_matrix[0][j][0]) &= ~((uint32_t)(1 << (31 - i)));
        (led_matrix[1][j][0]) &= ~((uint32_t)(1 << (31 - i)));
        (led_matrix[2][j][0]) &= ~((uint32_t)(1 << (31 - i)));
      } else {
        // clearing previous COLOR
        (led_matrix[0][j][1]) &= ~((uint32_t)(1 << (31 - (i - 32))));
        (led_matrix[1][j][1]) &= ~((uint32_t)(1 << (31 - (i - 32))));
        (led_matrix[2][j][1]) &= ~((uint32_t)(1 << (31 - (i - 32))));
      }
    }
  }
}
void initialize_display() {
  xTaskCreate(display_task_gpio, "display", 10000 / sizeof(void *), NULL, PRIORITY_HIGH, NULL);
}

void fill_rectangle(position a, position b, uint8_t color) {
  for (int x = a.x; x <= b.x; x++) {
    for (int y = a.y; y <= b.y; y++) {
      draw_pixel((position){x, y}, color);
    }
  }
}

void square_timeout_animation(position a, position b, uint8_t color) {
  int z = 1;
  for (int i = 0; i <= 5; i++) {
    for (int x = a.x; x <= b.x; x += z) {
      for (int y = a.y; y <= b.y; y += z) {
        draw_pixel((position){x, y}, color);
      }
    }
    z += 1;
    vTaskDelay(50);
    clear_rectangle(a, b);
  }
}
