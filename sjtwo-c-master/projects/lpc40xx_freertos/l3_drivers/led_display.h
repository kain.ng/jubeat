#pragma once
#include "stdint.h"
typedef struct {
  uint8_t x;
  uint8_t y;
} position;

void draw_pixel(position p, uint8_t color_8);
void initialize_display();
// void draw_circle(position p, uint16_t radius, uint8_t color);
void draw_animation(position p, uint8_t frame, uint8_t color);
void draw_64x64_frame_animation(uint8_t border_thickness, uint8_t color);
void draw_letter(position p, char c, uint8_t color);
void draw_string(position p, char *string, uint8_t color);
void draw_left_arrow(position p, uint8_t color);
void draw_right_arrow(position p, uint8_t color);
void clear_rectangle(position adj1, position adj2);
void fill_rectangle(position a, position b, uint8_t color);
void square_timeout_animation(position a, position b, uint8_t color);