#include "lpc40xx.h"
#include "stdint.h"

typedef enum {
  GPIO_INTR__FALLING_EDGE,
  GPIO_INTR__RISING_EDGE,
} gpio_interrupt_e;

typedef void (*function_pointer_t)(void);

// Allow the user to attach their callbacks for PORT0 and PORT2 with Rising edge or falling edge.
void gpio__attach_interrupt(uint8_t pin, uint8_t port, gpio_interrupt_e int_type, function_pointer_t callback);

// HEAD interrupt dispatcher for GPIO
void gpio__interrupt_dispatcher(void);

// clear the interrupt.
void clear_pin_interrupt(uint8_t bit, uint8_t port);

// get the pin no and port no of the pin that generated interrupt
uint8_t get_pin_port(uint8_t *port);